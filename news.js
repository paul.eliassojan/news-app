const url =
  "https://newsapi.org/v2/top-headlines?country=us&apiKey=05c0d68e9b6c4d41a1cf74a7d5f79211";

export async function getNews() {
  let result = await fetch(url).then(response => response.json());
  return result.articles;
}